#!/bin/bash

openssl rand -base64 700 > ./scripts/file.key
chmod 400 ./scripts/file.key 

docker-compose up -d

sleep 10

docker exec mongo-primary /scripts/setup.sh